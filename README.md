# SAS Temporal Manipulation Routines

## Overview

This project contains SAS programs that can be used to solve common
problems when dealing with temporal data.

* Collapsing a set of time spans that may be non-unique and overlapping
into a collection of contiguous non-overlapping spans.

* Taking a collection of contiguous spans of separate attributes and
converting those separate spans into spans of distinct collections of
associated attributes.

_Span_ - Indicates a span of time with a start (from) and end
(through) date.  In most of these examples, we will only be dealing
with date variables (no time-of-day).  In these cases, we use the
convention that the span begins at the beggining of the start date and
terminates at the end of the day of the end date.
