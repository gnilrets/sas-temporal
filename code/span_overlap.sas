/*************************************************************************

    NAME: Span Overlap
    
    DESCRIPTION:

    Contains SAS code use to take a set of possibly overlapping spans
    and convert them into the smallest number of contiguous spans possble.
    
    PURPOSE:

    To be used to clean up non-unique temporal span data.
    
    MACROS DEFINED:

    TAGS:

    TO BE DONE:

    OTHER:
    
*************************************************************************/





%macro span_overlap(
    INlds = , /* Input file */
    OUTlds = , /* Output file */
    vars_group = , /* Variable defining a distinct group */
    var_from = eff_from, /* Start date of span */
    var_thru = eff_thru, /* End date of span */
    worklib = WORK
    );


    * Make sure data is sorted by the grouping variables;
    proc sort data = &INlds
        (keep = &vars_group &var_from &var_thru)
        out = &worklib.._srt;
        by &vars_group &var_from &var_thru;
    run;

    data &OUTlds (drop = _:);
        set &worklib.._srt;
        by &vars_group;

        if _N_ = 1 then do;

            
            * Variables stored in hash, make sure lengths are preserved;
            if 0 then do;
                
                set &INlds
                (rename = (
                    &var_from = _h_from
                    &var_thru = _h_thru
                ));

                set &INlds
                (rename = (
                    &var_from = _d_date
                ));
                
            end;
            
            * Hash table to store each date per group;
            declare hash h_date(ordered:"a");
            h_date.definekey("_d_date");
            h_date.definedone();

            * Hash table to store individual spans;
            declare hash h_span(ordered:"a");
            h_span.definekey("_h_from","_h_thru");
            h_span.definedone();

            * Iterator objects needed for traversing hash tables;
            declare hiter hi_date("h_date");
            declare hiter hi_span("h_span");

        end;

        * Initialize hash objects on new group key;
        if first.%scan(&vars_group,-1,%str( )) then do;
            h_date.clear();
            h_span.clear();
        end;

        **  Add data to hash objects;
        _d_date = &var_from;
        h_date.ref();

        _d_date = &var_thru;
        h_date.ref();

        _h_from = &var_from;
        _h_thru = &var_thru;
        h_span.ref();



        * Initialize some more temp variables to preserve input key lengths;

        if 0 then do;

            set &INlds
            (rename = (
                &var_from = _span_from
                &var_thru = _span_thru
            ));
            
            set &INlds
            (rename = (
                &var_from = _p_date
            ));

            set &INlds
            (rename = (
                &var_from = _n_date
            ));
            
        end;

    
        ***   Construct contininuous spans   ***;
    
        if last.%scan(&vars_group,-1,%str( )) then do;

            ** Read the first start date (assumes no end < start);
            _rc_date = hi_date.first();
            _span_from = _d_date;

            * Mark this as the first day;
            _1st_date = 1;

            ** Read through the rest of the dates;
            do while(_rc_date = 0);

                * Determine if span is effective for this date;
                * by looping over all individual spans;
                
                _p_date = _d_date - 1; /* Define previous date */
                _n_date = _d_date + 1; /* Define next date */

                * Make the previous day behave as current if its the first;
                *  This handles 1-day spans;
                if _1st_date then
                    _p_date = _d_date;

                * Read first span;
                _rc_span = hi_span.first();

                _p_eff = 0;
                _d_eff = 0;
                _n_eff = 0;

                do while (_rc_span = 0);

                    * Determine if span was effective on previous, current, and next date;
                    _p_eff = _p_eff or (_p_date >= _h_from and _p_date <= _h_thru);
                    _d_eff = _d_eff or (_d_date >= _h_from and _d_date <= _h_thru);
                    _n_eff = _n_eff or (_n_date >= _h_from and _n_date <= _h_thru);

                    * Read next span;
                    _rc_span = hi_span.next();

                end; * Loop over all spans;



                * If effective changed between previous day and the current day,
                * then terminate previous span and start new one;

                if _p_eff ^= _d_eff then do;

                    _span_thru = _p_date;

                    &var_from = _span_from;
                    &var_thru = _span_thru;

                    * Output effective spans;
                    if &var_from <= &var_thru and _p_eff ^= 0 then
                        output;

                    * Start new span;
                    _span_from = _d_date;
                    _span_thru = .;            

                end;

                * If effective changed between current day and next one,;
                * then terminate current span and start a new one;

                if _d_eff ^= _n_eff then do;

                    _span_thru = _d_date;

                    &var_from = _span_from;
                    &var_thru = _span_thru;

                    if &var_from <= &var_thru and _d_eff ^= 0 then
                        output;

                    * Start new span;
                    _span_from = _n_date;
                    _span_thru = .;            

                end;

                * Move to next date point;
                _rc_date = hi_date.next();

                _1st_date = 0;

            end; * Loop over all dates;

            * Remove pointers;
            hi_date.last();
            _rc_date = hi_date.next();

            hi_span.last();
            _rc_span = hi_span.next();

        end;

    run;

%mend span_overlap;
