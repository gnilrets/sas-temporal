/*************************************************************************
    
    Program: $RCSfile: span_intersection.sas,v $
    $Revision: 1.1 $ / $Author: paramos $
    $Date: 2012/04/19 23:32:08 $

    Original Author: paramos 08-31-2011


    DESCRIPTION:    

    This macro will break up intersecting time spans
    into distinct spans where a set of dimensional variables is constant.
    It assumes that each dimension is single-valued (i.e., does not
    overlap with itself).  Each dimension must be referenced using
    a surrogate integer key.  Each dimension can come on a separate row,
    in which case the other dimensions should have a value of ZERO (0),
    indicating not applicable.


    EXAMPLE:
    
    A group has a funding type and a group size and both
    of these properties can change with time.  Of course, the group
    (or, at least, a specific plan in a group) can only have one group
    size and one funding type at a time, but they can have any
    combination of group size and funding type and they dont have to
    change at the same time.  Now suppose we have a group where both
    the funding type and the group size changed at different times.
    (For the sake of example, the natural keys, rather than the integer
    surrogate keys, are used here)
    

    Funding Type    Group Size   From         Thru
    -------------   ----------   ----------   ----------
    Fully Insured   N/A          01/01/2010   05/31/2010
    ASO             N/A          06/01/2010   12/31/2010

    
    Funding Type    Group Size   From         Thru
    -------------   ----------   ----------   ----------
    N/A             26-50        01/01/2010   03/31/2010
    N/A             51-100       04/01/2010   12/31/2010


    In this example, the group takes on three different combinations of
    the funding type and group size dimensions:


    Funding Type    Group Size   From         Thru
    -------------   ----------   ----------   ----------
    Fully Insured   26-50        01/01/2010   03/31/2010
    Fully Insured   51-100       04/01/2010   05/31/2010
    ASO             51-100       06/01/2010   12/31/2010

    Naturally, as the number of time-dependent dimensions increases,
    so does the complexity of the number of ways those dimensions can
    intersect.  The macro works by examining each potential intersection
    point (all distinct dates) and determining the value of all the
    dimensions on the day before, the day of, and the day after the
    intersection point.  If any of the dimensions have changed,
    then a span is created.

    Note that we interpret from dates as starting at the beginning of
    the day and thru dates ending at the end of the day.


    
    
    PARAMETERS:
        * INlds = Input library.dataset;
        * OUTlds - Output library.dataset;
        * vars_group - Intersecting spans are calculated within
            this list of variables (by processing);
        * var_from - Name of variable holding the from date;
        * var_thru - Name of variable holding the thru date;
        * vars_dim - Dimension variables (must be integers);


**************************************************************************
*************************************************************************/




%macro span_intersection(
    INlds = , /* Input file */
    OUTlds = , /* Output file */
    vars_group = gcp_nkti, /* Variable defining a distinct group */
    var_from = eff_from, /* Start date of span */
    var_thru = eff_thru, /* End date of span */
    vars_dim = gcp_nktd fdty_nk rtar_nk rxhq_nk, /* Dimension variables */
    worklib = WORK
    );


    %local n_vars_dim;    
    %let n_vars_dim = %sysfunc(countw(&vars_dim,%str( )));
    %do ivar = 1 %to &n_vars_dim;
        %let vars_dim_&ivar = %scan(&vars_dim,&ivar,%str( ));
        %put vars_dim_&ivar = &&vars_dim_&ivar;
    %end;



    * Make sure data is sorted by the grouping variables;
    proc sort data = &INlds
        (keep = &vars_group &var_from &var_thru &vars_dim)
        out = &worklib.._srt;
        by &vars_group &var_from &var_thru;
    run;

    data &OUTlds (drop = _:);
        set &worklib.._srt;
        by &vars_group;

        if _N_ = 1 then do;

            * Variables stored in hash, make sure lengths are preserved;
            if 0 then
                set &INlds
                (rename = (
                    %do ivar = 1 %to &n_vars_dim;
                        &&vars_dim_&ivar = _h_&&vars_dim_&ivar
                    %end;
                    &var_from = _h_from
                    &var_thru = _h_thru
                ));
            
            if 0 then
                set &INlds
                (rename = (
                    &var_from = _d_date
                ));
            
            call missing(
                %do ivar = 1 %to &n_vars_dim;
                    _h_&&vars_dim_&ivar,
                %end;
                _h_from,_h_thru,_d_date
            );


            * Hash table to store each date per member group key;
            declare hash h_date(ordered:"a");
            h_date.definekey("_d_date");
            h_date.definedone();

            * Hash table to store individual spans to be collapsed;
            declare hash h_span(ordered:"a");
            h_span.definekey(
                %do ivar = 1 %to &n_vars_dim;
                    "_h_&&vars_dim_&ivar",
                %end;
                "_h_from","_h_thru");
            h_span.definedone();

            * Iterator objects needed for traversing hash tables;
            declare hiter hi_date("h_date");
            declare hiter hi_span("h_span");

        end;


        * Initialize hash objects on new group key;
        if first.%scan(&vars_group,-1,%str( )) then do;
            h_date.clear();
            h_span.clear();
        end;

        * Add values to hash objects;
        _d_date = &var_from;
        h_date.ref();

        _d_date = &var_thru;
        h_date.ref();

        %do ivar = 1 %to &n_vars_dim;
            _h_&&vars_dim_&ivar = &&vars_dim_&ivar;
        %end;
        _h_from = &var_from;
        _h_thru = &var_thru;
        h_span.ref();

        *** Process overlapping dimensions once all spans have been read;

        * Initialize some more temp variables to preserve input key lengths;

        if 0 then do;
            
            set &INlds
            (rename = (
                &var_from = _span_from
                &var_thru = _span_thru
            ));
            
            set &INlds
            (rename = (
                &var_from = _p_date
            ));

            set &INlds
            (rename = (
                &var_from = _n_date
            ));

            set &INlds
            (rename = (
                %do ivar = 1 %to &n_vars_dim;
                    &&vars_dim_&ivar = _p_&&vars_dim_&ivar
                %end;
            ));

            set &INlds
            (rename = (
                %do ivar = 1 %to &n_vars_dim;
                    &&vars_dim_&ivar = _d_&&vars_dim_&ivar
                %end;
            ));

            set &INlds
            (rename = (
                %do ivar = 1 %to &n_vars_dim;
                    &&vars_dim_&ivar = _n_&&vars_dim_&ivar
                %end;
            ));
            
        end;
            
        call missing(
            %do ivar = 1 %to &n_vars_dim;
                _p_&&vars_dim_&ivar,
                _d_&&vars_dim_&ivar,
                _n_&&vars_dim_&ivar,                    
            %end;
            _span_from,_span_thru,_p_date,_d_date,_n_date
        );


        if last.%scan(&vars_group,-1,%str( )) then do;

            ** Read the first start date (assumes no end < start);
            _rc_date = hi_date.first();
            _span_from = _d_date;

            * Mark this as the first day;
            _1st_date = 1;
            
            ** Read through the rest of the dates;
            do while(_rc_date = 0);

                * Determine which dimensions are populated for this date;
                * by looping over all individual spans;

                _p_date = _d_date - 1;
                _n_date = _d_date + 1;

                * Make the previous day behave as current if its the first;
                *  This handles 1-day spans;
                if _1st_date then
                    _p_date = _d_date;
                
                call missing(
                    %do ivar = 1 %to &n_vars_dim;
                        _p_&&vars_dim_&ivar,
                        _d_&&vars_dim_&ivar,
                        _n_&&vars_dim_&ivar
                        %if &ivar ^= &n_vars_dim %then ,;
                    %end;
                );

                * Read first span;
                _rc_span = hi_span.first();

                do while (_rc_span = 0);

                    * Find dimensions on previous date;
                    if (_p_date >= _h_from and _p_date <= _h_thru) then do;
                        %do ivar = 1 %to &n_vars_dim;

                            if sum(0,_p_&&vars_dim_&ivar)
                                and sum(0,_h_&&vars_dim_&ivar) then do;
                                call execute('%error(Multivalued dimension)');
                                put _ALL_;
                                stop;
                            end;
                                
                            _p_&&vars_dim_&ivar = sum(_p_&&vars_dim_&ivar,
                                _h_&&vars_dim_&ivar);

                        %end;
                    end;

                    * Find dimensions on current date;
                    if (_d_date >= _h_from and _d_date <= _h_thru) then do;
                        %do ivar = 1 %to &n_vars_dim;

                            if sum(0,_d_&&vars_dim_&ivar)
                                and sum(0,_h_&&vars_dim_&ivar) then do;
                                call execute('%error(Multivalued dimension)');
                                put _ALL_;
                                stop;
                            end;
                                
                            _d_&&vars_dim_&ivar = sum(_d_&&vars_dim_&ivar,
                                _h_&&vars_dim_&ivar);
                            
                        %end;
                    end;

                    * Find dimensions on next date;
                    if (_n_date >= _h_from and _n_date <= _h_thru) then do;
                        %do ivar = 1 %to &n_vars_dim;
                        
                            if sum(0,_n_&&vars_dim_&ivar)
                                and sum(0,_h_&&vars_dim_&ivar) then do;
                                call execute('%error(Multivalued dimension)');
                                put _ALL_;
                                stop;
                            end;
                                
                            _n_&&vars_dim_&ivar = sum(_n_&&vars_dim_&ivar,
                                _h_&&vars_dim_&ivar);
                            
                        %end;
                    end;

                    * Read next span;
                    _rc_span = hi_span.next();
                
                end; * Loop over all spans;



                * If dimensions changed between previous day and the current day,
                * then terminate previous span and start new one;

                if 
                    %do ivar = 1 %to &n_vars_dim;
                        _p_&&vars_dim_&ivar ^= _d_&&vars_dim_&ivar
                        %if &ivar ^= &n_vars_dim %then or;
                    %end; then do;

                    _span_thru = _p_date;
                    
                    %do ivar = 1 %to &n_vars_dim;
                        &&vars_dim_&ivar = _p_&&vars_dim_&ivar;
                    %end;

                    &var_from = _span_from;
                    &var_thru = _span_thru;

                    * A span gap would result in null values, change to zeros;
                    %do ivar = 1 %to &n_vars_dim;
                        &&vars_dim_&ivar = sum(0,&&vars_dim_&ivar);
                    %end;
                    
                    * Output valid spans that are populated by at least one dimension var;
                    if &var_from <= &var_thru
                        and sum(0
                            %do ivar = 1 %to &n_vars_dim;
                                ,&&vars_dim_&ivar
                            %end;) ^= 0
                        then
                        output;

                    * Start new span;
                    _span_from = _d_date;
                    _span_thru = .;            
                        
                end;

                * If dimensions changed between current day and next one,;
                * then terminate current span and start a new one;

                if 
                    %do ivar = 1 %to &n_vars_dim;
                        _d_&&vars_dim_&ivar ^= _n_&&vars_dim_&ivar
                        %if &ivar ^= &n_vars_dim %then or;
                    %end; then do;

                    _span_thru = _d_date;
                    
                    %do ivar = 1 %to &n_vars_dim;
                        &&vars_dim_&ivar = _d_&&vars_dim_&ivar;
                    %end;

                    &var_from = _span_from;
                    &var_thru = _span_thru;

                    * A span gap would result in null values, change to zeros;
                    %do ivar = 1 %to &n_vars_dim;
                        &&vars_dim_&ivar = sum(0,&&vars_dim_&ivar);
                    %end;

                    * Output valid spans that are populated by at least one dimension var;
                    if &var_from <= &var_thru
                        and sum(0
                            %do ivar = 1 %to &n_vars_dim;
                                ,&&vars_dim_&ivar
                            %end;) ^= 0
                        then
                        output;

                    * Start new span;
                    _span_from = _n_date;
                    _span_thru = .;            
                        
                end;
                
                * Move to next date point;
                _rc_date = hi_date.next();

                _1st_date = 0;
                
            end; * Loop over all dates;

            * Remove pointers;
            hi_date.last();
            _rc_date = hi_date.next();

            hi_span.last();
            _rc_span = hi_span.next();
            
        end; * Last.dot processing;            
    
    run;

%mend span_intersection;








/**********************************************************************


    Macro: span_overlap

    Parameters: See below;

    Input tables:
        
    Output tables:
    
    Description:

        This macro collapses all overlaping spans into a single
        contiguous span.
        

    Original author: paramos 2-22-2012

***********************************************************************/



%macro span_overlap(
    INlds = , /* Input file */
    OUTlds = , /* Output file */
    vars_group = gcp_nkti, /* Variable defining a distinct group */
    var_from = eff_from, /* Start date of span */
    var_thru = eff_thru, /* End date of span */
    worklib = WORK
    );


    * Make sure data is sorted by the grouping variables;
    proc sort data = &INlds
        (keep = &vars_group &var_from &var_thru)
        out = &worklib.._srt;
        by &vars_group &var_from &var_thru;
    run;

    data &OUTlds (drop = _:);
        set &worklib.._srt;
        by &vars_group;

        if _N_ = 1 then do;

            
            * Variables stored in hash, make sure lengths are preserved;
            if 0 then do;
                
                set &INlds
                (rename = (
                    &var_from = _h_from
                    &var_thru = _h_thru
                ));

                set &INlds
                (rename = (
                    &var_from = _d_date
                ));
                
            end;
            
            * Hash table to store each date per group;
            declare hash h_date(ordered:"a");
            h_date.definekey("_d_date");
            h_date.definedone();

            * Hash table to store individual spans;
            declare hash h_span(ordered:"a");
            h_span.definekey("_h_from","_h_thru");
            h_span.definedone();

            * Iterator objects needed for traversing hash tables;
            declare hiter hi_date("h_date");
            declare hiter hi_span("h_span");

        end;

        * Initialize hash objects on new group key;
        if first.%scan(&vars_group,-1,%str( )) then do;
            h_date.clear();
            h_span.clear();
        end;

        **  Add data to hash objects;
        _d_date = &var_from;
        h_date.ref();

        _d_date = &var_thru;
        h_date.ref();

        _h_from = &var_from;
        _h_thru = &var_thru;
        h_span.ref();



        * Initialize some more temp variables to preserve input key lengths;

        if 0 then do;

            set &INlds
            (rename = (
                &var_from = _span_from
                &var_thru = _span_thru
            ));
            
            set &INlds
            (rename = (
                &var_from = _p_date
            ));

            set &INlds
            (rename = (
                &var_from = _n_date
            ));
            
        end;

    
        ***   Construct contininuous spans   ***;
    
        if last.%scan(&vars_group,-1,%str( )) then do;

            ** Read the first start date (assumes no end < start);
            _rc_date = hi_date.first();
            _span_from = _d_date;

            * Mark this as the first day;
            _1st_date = 1;

            ** Read through the rest of the dates;
            do while(_rc_date = 0);

                * Determine if span is effective for this date;
                * by looping over all individual spans;
                
                _p_date = _d_date - 1;
                _n_date = _d_date + 1;

                * Make the previous day behave as current if its the first;
                *  This handles 1-day spans;
                if _1st_date then
                    _p_date = _d_date;

                * Read first span;
                _rc_span = hi_span.first();

                _p_eff = 0;
                _d_eff = 0;
                _n_eff = 0;

                do while (_rc_span = 0);

                    * Determine if span was effective on previous, current, and next date;
                    _p_eff = _p_eff or (_p_date >= _h_from and _p_date <= _h_thru);
                    _d_eff = _d_eff or (_d_date >= _h_from and _d_date <= _h_thru);
                    _n_eff = _n_eff or (_n_date >= _h_from and _n_date <= _h_thru);

                    * Read next span;
                    _rc_span = hi_span.next();

                end; * Loop over all spans;



                * If effective changed between previous day and the current day,
                * then terminate previous span and start new one;

                if _p_eff ^= _d_eff then do;

                    _span_thru = _p_date;

                    &var_from = _span_from;
                    &var_thru = _span_thru;

                    * Output effective spans;
                    if &var_from <= &var_thru and _p_eff ^= 0 then
                        output;

                    * Start new span;
                    _span_from = _d_date;
                    _span_thru = .;            

                end;

                * If effective changed between current day and next one,;
                * then terminate current span and start a new one;

                if _d_eff ^= _n_eff then do;

                    _span_thru = _d_date;

                    &var_from = _span_from;
                    &var_thru = _span_thru;

                    if &var_from <= &var_thru and _d_eff ^= 0 then
                        output;

                    * Start new span;
                    _span_from = _n_date;
                    _span_thru = .;            

                end;

                * Move to next date point;
                _rc_date = hi_date.next();

                _1st_date = 0;

            end; * Loop over all dates;

            * Remove pointers;
            hi_date.last();
            _rc_date = hi_date.next();

            hi_span.last();
            _rc_span = hi_span.next();

        end;

    run;

%mend span_overlap;
