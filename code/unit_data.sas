/*************************************************************************

    NAME: Unit Data
    
    DESCRIPTION:
    
    Contains data to unit test the temporal manipulation routines.
    
    PURPOSE:

    To set up some standardized common data for testing and development.
    
    
    MACROS DEFINED:

    TAGS:

    TO BE DONE:

    OTHER:
    
*************************************************************************/


data test;

    length
        id $5
        from_date 8
        thru_date 8
        ;

    format
        from_date YYMMDDN8.
        thru_date YYMMDDN8.
        ;

    informat
        from_date YYMMDD8.
        thru_date YYMMDD8.
        ;

    input
        id
        from_date
        thru_date
        ;

    infile datalines dsd dlm='|';
    datalines;
00001|20120101|20120131
00001|20120201|20120331
00001|20120310|20120430
00001|20120501|20120701
00001|20120703|20121231
00002|20120110|20120110
00002|20120111|20120131
00002|20120201|20120331
;
run;


